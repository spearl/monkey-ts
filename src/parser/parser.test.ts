import { LetStatement } from '../ast'
import { Lexer } from '../lexer'
import { Parser } from './parser'

describe("Parser", () => {
  it("should parse let statements", () => {
    const input = `
let x = 5;
let y = 10;
let foobar = 838383;
`
    const lex = new Lexer(input)
    const p = new Parser(lex)

    const program = p.parseProgram()
    expect(p.errors.length).toBe(0)
    expect(program).toBeTruthy()
    expect(program.statements.length).toBe(3)

    const tests: {expectedIdentifier: string}[] = [
        {expectedIdentifier: "x"},
        {expectedIdentifier: "y"},
        {expectedIdentifier: "foobar"}
    ]
    
    tests.forEach((tt, i) => {
        let stmt = program.statements[i]
        let name = tt.expectedIdentifier

        expect(stmt.tokenLiteral()).toBe("let")
        expect(stmt).toBeInstanceOf(LetStatement)
        const letStmt = <LetStatement> stmt
        expect(letStmt.name.value).toBe(name)
        expect(letStmt.name.tokenLiteral()).toBe(name)
    })
  })
})
