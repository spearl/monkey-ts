import { Lexer } from '../lexer'
import { LetStatement, Program, Identifier } from '../ast'
import * as token from '../token'

export class Parser {
    lex: Lexer

    errors: string[] = []

    curToken: token.Token
    peekToken: token.Token

    constructor(lex: Lexer) {
        this.lex = lex
        this.curToken = this.lex.nextToken()
        this.peekToken = this.lex.nextToken()
    }

    nextToken() {
        this.curToken = this.peekToken
        this.peekToken = this.lex.nextToken()
    }

    parseProgram() {
        const program = new Program()

        while (this.curToken.type !== token.EOF) {
            const stmt = this.parseStatement()
            if (stmt !== null) {
                program.statements.push(stmt)
            }
            this.nextToken()
        }

        return program
    }

    parseStatement() {
        switch (this.curToken.type) {
            case token.LET:
                return this.parseLetStatement()
            default:
                return null
        }
    }

    parseLetStatement() {
        const letToken = this.curToken

        if (!this.expectPeek(token.IDENT)) {
            return null
        }
        
        const name = new Identifier(this.curToken, this.curToken.literal)

        if (!this.expectPeek(token.ASSIGN)) {
            return null
        }

        // TODO: skipping expressions until semicolon
        while (!this.curTokenIs(token.SEMICOLON)) {
            this.nextToken()
        }

        return new LetStatement(letToken, name)
    }

    curTokenIs = (t: token.TokenType) => this.curToken.type === t
    
    peekTokenIs = (t: token.TokenType) => this.peekToken.type === t
    
    expectPeek(t: token.TokenType) {
        if (this.peekTokenIs(t)) {
            this.nextToken()
            return true
        } else {
            this.peekError(t)
            return false
        }
    }

    peekError(t: token.TokenType) {
        const msg = `expected next token to be ${t}, got ${this.peekToken.type}`
        this.errors.push(msg)
    }
}