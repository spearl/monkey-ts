import * as repl from 'repl'

import { Lexer } from '../lexer'
import * as token from '../token'

const evalLine: repl.REPLEval = (line, context, filename, callback) => {
  const l = new Lexer(line)
  
  for (let tok = l.nextToken(); tok.type !== token.EOF; tok = l.nextToken()) {
    console.log(tok)
  }
  callback(null, null)
}

export function start(inStream: NodeJS.ReadStream, outStream: NodeJS.WriteStream) {
  repl.start({
    prompt: ">> ",
    input: inStream,
    output: outStream,
    eval: evalLine
  })
}
