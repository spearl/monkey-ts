import os from 'os'

import { start } from './repl'

function main() {
  const user = os.userInfo().username
  console.log(`Hello ${user}! This is the Monkey programming language!`)
  console.log("Feel free to type in commands\n")
  start(process.stdin, process.stdout)
}

main()