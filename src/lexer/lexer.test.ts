import { Lexer } from "./lexer"
import * as token from "../token"

type testCase = {
  expectedType: token.TokenType
  expectedLiteral: string
}

function c(expectedType: token.TokenType, expectedLiteral: string): testCase {
  return {
    expectedType,
    expectedLiteral
  }
}

describe("Lex Tokens", () => {
  it("should return expected tokens and literals", () => {
    const input = `let five = 5;
let ten = 10;

let add = fn(x, y) {
    x + y;
};

let result = add(five, ten);
!-/*5;
5 < 10 > 5;

if (5 < 10) {
    return true;
} else {
    return false;
}

10 == 10;
10 != 9;
`

    const tests = [
      c(token.LET, "let"),
      c(token.IDENT, "five"),
      c(token.ASSIGN, "="),
      c(token.INT, "5"),
      c(token.SEMICOLON, ";"),
      c(token.LET, "let"),
      c(token.IDENT, "ten"),
      c(token.ASSIGN, "="),
      c(token.INT, "10"),
      c(token.SEMICOLON, ";"),
      c(token.LET, "let"),
      c(token.IDENT, "add"),
      c(token.ASSIGN, "="),
      c(token.FUNCTION, "fn"),
      c(token.LPAREN, "("),
      c(token.IDENT, "x"),
      c(token.COMMA, ","),
      c(token.IDENT, "y"),
      c(token.RPAREN, ")"),
      c(token.LBRACE, "{"),
      c(token.IDENT, "x"),
      c(token.PLUS, "+"),
      c(token.IDENT, "y"),
      c(token.SEMICOLON, ";"),
      c(token.RBRACE, "}"),
      c(token.SEMICOLON, ";"),
      c(token.LET, "let"),
      c(token.IDENT, "result"),
      c(token.ASSIGN, "="),
      c(token.IDENT, "add"),
      c(token.LPAREN, "("),
      c(token.IDENT, "five"),
      c(token.COMMA, ","),
      c(token.IDENT, "ten"),
      c(token.RPAREN, ")"),
      c(token.SEMICOLON, ";"),
      c(token.BANG, "!"),
      c(token.MINUS, "-"),
      c(token.SLASH, "/"),
      c(token.ASTERISK, "*"),
      c(token.INT, "5"),
      c(token.SEMICOLON, ";"),
      c(token.INT, "5"),
      c(token.LT, "<"),
      c(token.INT, "10"),
      c(token.GT, ">"),
      c(token.INT, "5"),
      c(token.SEMICOLON, ";"),
      c(token.IF, "if"),
      c(token.LPAREN, "("),
      c(token.INT, "5"),
      c(token.LT, "<"),
      c(token.INT, "10"),
      c(token.RPAREN, ")"),
      c(token.LBRACE, "{"),
      c(token.RETURN, "return"),
      c(token.TRUE, "true"),
      c(token.SEMICOLON, ";"),
      c(token.RBRACE, "}"),
      c(token.ELSE, "else"),
      c(token.LBRACE, "{"),
      c(token.RETURN, "return"),
      c(token.FALSE, "false"),
      c(token.SEMICOLON, ";"),
      c(token.RBRACE, "}"),
      c(token.INT, "10"),
      c(token.EQ, "=="),
      c(token.INT, "10"),
      c(token.SEMICOLON, ";"),
      c(token.INT, "10"),
      c(token.NOT_EQ, "!="),
      c(token.INT, "9"),
      c(token.SEMICOLON, ";"),
      c(token.EOF, "")
    ]

    const l = new Lexer(input)

    tests.forEach((tt, i) => {
      let tok = l.nextToken()

      expect(tok.type).toBe(tt.expectedType)
      expect(tok.literal).toBe(tt.expectedLiteral)
    })
  })
})