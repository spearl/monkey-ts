// Lexer
import * as token from '../token/token'
import { isTemplateLiteral } from '@babel/types';

type ReadChar = string | null

export class Lexer {
  input: string
  ch: ReadChar
  position = 0
  readPosition = 0

  constructor(input: string) {
    this.input = input
    this.ch = null

    this.readChar()
  }

  nextToken() {
    let tok: token.Token

    this.skipWhitespace()

    switch (this.ch) {
      case '=':
        if (this.peekChar() == '=') {
          const ch = this.ch
          this.readChar()
          const literal = ch.concat(this.ch)
          tok = newToken(token.EQ, literal)
        } else {
          tok = newToken(token.ASSIGN, this.ch)
        }
        break
      case '+':
        tok = newToken(token.PLUS, this.ch)
        break
      case '-':
        tok = newToken(token.MINUS, this.ch)
        break
      case '!':
        if (this.peekChar() == '=') {
          const ch = this.ch
          this.readChar()
          const literal = ch.concat(this.ch)
          tok = newToken(token.NOT_EQ, literal)
        } else {
          tok = newToken(token.BANG, this.ch)
        }
        break
      case '/':
        tok = newToken(token.SLASH, this.ch)
        break
      case '*':
        tok = newToken(token.ASTERISK, this.ch)
        break
      case '<':
        tok = newToken(token.LT, this.ch)
        break
      case '>':
        tok = newToken(token.GT, this.ch)
        break
      case ';':
        tok = newToken(token.SEMICOLON, this.ch)
        break
      case '(':
        tok = newToken(token.LPAREN, this.ch)
        break
      case ')':
        tok = newToken(token.RPAREN, this.ch)
        break
      case ',':
        tok = newToken(token.COMMA, this.ch)
        break
      case '{':
        tok = newToken(token.LBRACE, this.ch)
        break
      case '}':
        tok = newToken(token.RBRACE, this.ch)
        break
      case null:
        tok = {
          literal: "",
          type: token.EOF
        }
        break
      default:
        if (isLetter(this.ch)) {
          const literal = this.readIdentifier()
          tok = {
            literal,
            type: token.lookupIdent(literal)
          }
          return tok
        } else if (isDigit(this.ch)) {
          tok = {
            literal: this.readNumber(),
            type: token.INT
          }
          return tok
        } else {
          tok = newToken(token.ILLEGAL, this.ch)
        }
    }

    this.readChar()
    return tok
  }

  peekChar() {
    if (this.readPosition >= this.input.length) {
      return null
    }
    return this.input[this.readPosition]
  }

  readChar() {
    if (this.readPosition >= this.input.length) {
      this.ch = null
    } else {
      this.ch = this.input[this.readPosition]
    }
    this.position = this.readPosition
    this.readPosition++
  }

  readIdentifier() {
    const position = this.position
    while (isLetter(this.ch)) {
      this.readChar()
    }
    return this.input.slice(position, this.position)
  }

  readNumber() {
    const position = this.position
    while (isDigit(this.ch)) {
      this.readChar()
    }
    return this.input.slice(position, this.position)
  }

  skipWhitespace() {
    while (this.ch && this.ch.match(/\s/)) {
      this.readChar()
    }
  }
}

function isLetter(ch: ReadChar) {
  return Boolean(ch && ch.match(/[a-z_]/i))
}

function isDigit(ch: ReadChar) {
  return Boolean(ch && ch.match(/[0-9]/))
}

function newToken(tokenType: token.TokenType, ch: string): token.Token {
  return {
    type: tokenType,
    literal: ch
  }
}