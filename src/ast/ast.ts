import * as token from '../token'

interface ASTNode {
    tokenLiteral(): string
}

export interface Statement extends ASTNode {}

export interface Expression extends ASTNode {}

export class Program {
    statements: Statement[] = []

    tokenLiteral() {
        if (this.statements.length > 0) {
            return this.statements[0].tokenLiteral()
        } else {
            return ""
        }
    }
}

export class LetStatement implements Statement {
    token: token.Token // the token.LET token
    name: Identifier
    value?: Expression

    constructor(token: token.Token, name: Identifier) {
        this.token = token
        this.name = name
    }
    
    tokenLiteral() {
        return this.token.literal
    }
}

export class Identifier implements Expression {
    token: token.Token // the token.IDENT token
    value: string

    constructor(token: token.Token, value: string) {
        this.token = token
        this.value = value
    }

    tokenLiteral() {
        return this.token.literal
    }
}